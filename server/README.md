This server opens a websocket to comunicate with the clients.

It also stores the ranking data in a sqlite database.

Requires Python 3.6

## Install

    pip install -r requirements.txt

## Run

    python main.py


## Hosting

install: virtualenv, supervisor, rsync, sudo

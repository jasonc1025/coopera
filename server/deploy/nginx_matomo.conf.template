# Based on:
# https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04

server {
    listen 80;
    listen [::]:80;
    server_name %(matomo_domain)s;
    return 301 https://$server_name$request_uri;
}

server {

    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name %(matomo_domain)s;
    client_max_body_size 10M;
    keepalive_timeout    15;
    error_log %(log_folder)s/matomo_error_nginx.log info;

    include %(proj_name)s_ssl_params;

    root /var/www/piwik;

    # Deny illegal Host headers
    if ($host !~* ^(%(matomo_domain)s)$) {
        return 444;
    }

    location / {
            # First attempt to serve request as file, then
            # as directory, then fall back to displaying a 404.
            try_files $uri $uri/ =404;
    }

    # pass PHP scripts to FastCGI server
    location ~ \.php$ {
            include snippets/fastcgi-php.conf;
            # With php-fpm (or other unix sockets):
            fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
    }
}

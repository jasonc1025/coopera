from coopera_server.utils import now


class Chat(object):

    def __init__(self):
        self.messages = []

    def add_msg(self, player: object, msg: str):
        msg_obj = {
            'player': player.name,
            'msg': msg,
            'date': now(),
        }
        self.messages.append(msg_obj)
        return msg_obj

    def to_json(self):
        return self.messages

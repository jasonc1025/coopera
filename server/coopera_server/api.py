import re

import bleach
from flask import request, session
from flask_socketio import emit, Namespace, join_room, leave_room

from coopera_server.app import socketio

from coopera_server.game import game, NameExists


def get_player_name():
    name = session.get('player_name')
    return name if name else request.sid


def safe_string(string) -> bool:
    '''
    Check if a string is safe.
    '''
    return True if re.fullmatch('[a-zA-Z0-9]{1,20}', string) else False


class ApiNamespace(Namespace):

    def send_msg(self, _type, text):
        msg = {'type': _type, 'text': text}
        emit('server_msg', msg)

    def error_name_exists(self):
        self.send_msg('error', 'name already exists')

    def error_name_invalid(self):
        self.send_msg('error', 'invalid name')

    def on_connect(self):
        name = get_player_name()
        game.create_player(name)
        session['player_name'] = name

    def on_set_player_name(self, name: str):
        if not safe_string(name):
            self.error_name_invalid()
            return

        try:
            old_name = session.get('player_name')
            if old_name:
                game.rename_player(old_name, name)
            else:
                game.create_player(name)
            session['player_name'] = name
            emit('update_player_name', name)
        except NameExists:
            self.error_name_exists()

    def on_disconnect(self):
        game.remove_player(get_player_name())

    def on_validate_room_name(self, room_name: str):
        '''
        Validates a game room name. Name cannot exist.
        '''
        if not safe_string(room_name):
            self.error_name_invalid()
            return

        if not game.check_new_room_name(room_name):
            self.error_name_exists()
            return

        emit('room_name_validated', room_name)

    def on_send_player_msg(self, msg: str):
        msg = bleach.clean(msg)
        game.add_chat_msg(get_player_name(), msg)

    def on_join_game_room(self, room_name: str):
        '''
        Join a game room. Create if doesn't exist.
        '''
        if not safe_string(room_name):
            self.error_name_invalid()
            return

        try:
            game.create_room(room_name)
        except NameExists:
            pass
        game.add_player_to_room(get_player_name(), room_name)

    def on_leave_game_room(self):
        game.remove_player_from_room(get_player_name())

    def on_join_rooms_list(self):
        emit('update_rooms_list', game.get_rooms_list_data())
        join_room('>rooms_list')

    def on_leave_rooms_list(self):
        leave_room('>rooms_list')

    def on_set_player_command(self, command: str):
        game.set_player_command(get_player_name(), command)

    def on_join_rank_list(self):
        emit('update_rank_list', game.get_rank_list_data())
        join_room('>rank_list')

    def on_leave_rank_list(self):
        leave_room('>rank_list')


socketio.on_namespace(ApiNamespace('/'))

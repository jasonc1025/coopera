from flask_socketio import emit, join_room, leave_room

from coopera_server.app import socketio
from coopera_server.rank import RankList
from coopera_server.chat import Chat
from coopera_server.utils import now
from coopera_server.map import generate_map


# number of turns to wait after one level ends before starting the new level
AWAIT_AFTER_GAME_END = 2


class NameExists(Exception):
    pass


class GamePlayer(object):
    '''A player.'''

    def __init__(self, name: str):
        self.name = name
        self.room = None
        self.command = None

    def set_command(self, command: str):
        '''Set the command for a turn.'''
        self.command = command

    def get_command(self) -> str:
        '''Get command for a turn.'''
        return self.command

    def set_name(self, name: str):
        '''Set player name.'''
        self.name = name

    def get_name(self) -> str:
        '''Get player name.'''
        return self.name

    def join_room(self, room):
        '''This player join a room. `room` is a GameRoom.'''
        self.leave_room()
        self.room = room
        room.add_player(self)

    def leave_room(self):
        '''This player leaves the room.'''
        room = self.room
        self.room = None
        if room:
            room.remove_player(self)
            leave_room(room.name)
        return room

    def rename(self, name):
        '''Rename this player.'''
        self.name = name

    def to_json(self):
        '''Return this player data has JSON.'''
        return {
            'name': self.name,
            'command': self.command
        }


class GameRoom(object):
    '''A game room.'''

    def __init__(self, name: str, game_manager):
        self.name = name
        self.game_manager = game_manager
        self.current_level = 1
        self.chat = Chat()
        self.players = []
        self.creation_date = now()
        self.generate_map()
        self.alive = True
        self.turn_wait = 2
        self.theard = socketio.start_background_task(self.run)

    def info(self) -> dict:
        '''Returns a dict with basic info about the room.'''
        return {
            'name': self.name,
            'level': self.current_level,
            'players': [player.name for player in self.players],
            'creation': self.creation_date
        }

    def empty(self) -> bool:
        '''Check if this room is empty.'''
        return not len(self.players)

    def destroy(self):
        '''Set the flag that will kill this room thread.'''
        self.alive = False

    def generate_map(self):
        '''Generate a new map based on this room level.'''
        self.map = generate_map(self.current_level)

    def add_chat_msg(self, player, msg):
        '''Add a chat msg for a player in this room.'''
        msg_obj = self.chat.add_msg(player, msg)
        socketio.emit('add_chat_msg', msg_obj, room=self.name)

    def to_json(self, rank_data=None):
        '''Return this room data as JSON.'''
        return {
            'players': [player.to_json() for player in self.players],
            'map': self.map.to_json(),
            'level': self.current_level,
            'rank': rank_data
        }

    def run(self):
        '''Method that will be run by this room thread.'''
        already_ranked = False
        rank_data = None
        while self.alive:
            socketio.sleep(self.turn_wait)

            if not self.alive:
                return

            self.map.run_turn(self.players)

            if self.map.ended() is not False:
                # rank the game only once
                if not already_ranked and self.map.lost:
                    rank_data = self.game_manager.try_update_rank(
                        self.current_level, self.players)
                    already_ranked = True

                if self.map.ended() > AWAIT_AFTER_GAME_END:
                    if self.map.won:
                        self.current_level += 1
                    elif self.map.lost:
                        self.current_level -= 1
                        if self.current_level < 1:
                            self.current_level = 1
                    self.generate_map()
                    already_ranked = False
                    rank_data = None

            if not self.alive:
                return

            socketio.emit(
                'update_game_room', self.to_json(rank_data), room=self.name)

    def add_player(self, player: GamePlayer):
        '''Add a player to this room.'''
        if player not in self.players:
            self.players.append(player)

    def remove_player(self, player: GamePlayer):
        '''Remove a player from this room.'''
        self.players.remove(player)
        if self.empty():
            self.game_manager.remove_room(self.name)


class GameManager(object):
    '''Main class that controls the data in the server.'''

    def __init__(self):
        self.rooms = {}
        self.players = {}
        self.rank = RankList()

    def try_update_rank(self, level, players):
        '''Try to update the rank of a group of players.'''
        return self.rank.try_update(level, players)

    def get_rooms_list_data(self) -> list:
        '''Return the data from the rooms list.'''
        return {
            'rooms': [room.info() for room in self.rooms.values()],
            'num_players': len(self.players)
        }

    def get_rank_list_data(self) -> dict:
        '''Return the data from the ranking as JSON.'''
        return self.rank.to_json()

    # def get_game_room_data(self, name: str) -> dict:
    #     '''Return the data from a room as JSON.'''
    #     return self.rooms[name].to_json()

    def get_chat_data(self, name: str) -> dict:
        '''Return the chat data from a room as JSON.'''
        return self.rooms[name].chat.to_json()

    def create_room(self, name: str) -> GameRoom:
        '''Create a new room.'''
        if self.rooms.get(name):
            raise NameExists

        room = GameRoom(name, self)
        self.rooms[name] = room
        self.update_rooms_list()
        return room

    def check_new_room_name(self, room_name: str) -> bool:
        '''Check if a room name is new.'''
        if room_name in self.rooms:
            return False
        else:
            return True

    def remove_room(self, name: str):
        '''Remove a room from the server.'''
        room = self.rooms.pop(name)
        self.update_rooms_list()
        room.destroy()

    def update_rooms_list(self):
        '''Send data about the rooms to all players in the `>rooms_list`
        channel.'''
        socketio.emit(
            'update_rooms_list',
            self.get_rooms_list_data(),
            room='>rooms_list')

    def create_player(self, name: str):
        '''Create a new player.'''
        if self.players.get(name):
            raise NameExists
        self.players[name] = GamePlayer(name)
        self.update_rooms_list()

    def rename_player(self, old_name: str, new_name: str):
        '''Rename a player.'''
        if self.players.get(new_name):
            raise NameExists
        player = self.players.pop(old_name)
        player.rename(new_name)
        self.players[new_name] = player

    def add_chat_msg(self, player_name: str, msg: str):
        '''Add a chat msg to a game room.'''
        player = self.players[player_name]
        player.room.add_chat_msg(player, msg)

    def remove_player(self, name: str):
        '''Remove player from server.'''
        self.remove_player_from_room(name)
        self.players.pop(name)
        self.update_rooms_list()

    def set_player_command(self, player: str, command: str):
        '''Set the player command for this turn.'''
        self.players[player].set_command(command)

    def add_player_to_room(self, player_name: str, room_name: str):
        '''Add a player to a room.'''
        room = self.rooms[room_name]
        player = self.players[player_name]
        player.join_room(room)
        join_room(room_name)
        # emit('update_game_room', self.get_game_room_data(room_name))
        emit('update_game_room', room.to_json())
        self.update_rooms_list()

    def remove_player_from_room(self, player_name: str):
        '''Remove player from its current room.'''
        self.players[player_name].leave_room()


game = GameManager()

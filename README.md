# Coopera

![Coopera](https://gitlab.com/andresmrm/coopera/raw/master/client/screen.png) 

A cooperative multiplayer game where all players command the same avatar!

Play: https://coopera.ikotema.digital

## Technical

Made with [VueJS](https://vuejs.org) and [Python Flask](http://flask.pocoo.org). Check the README.md in the server and client folders for more information.

Audio project made with [LMMS](https://lmms.io).

The fabfile in this folder may help installing and deploying the server.

## Inspiration

After starting the game I realize that I was probably unconsciously influenced by:

- These [games about social behavior with simple graphics](http://ncase.me)
- These web multiplayer games with simple graphics: [Agar.io](http://agar.io) and [Diep.io](http://diep.io)
- This [color pallete](http://ethanschoonover.com/solarized) (vaguely)

## Credits

Made by [Andrés M. R. Martano](https://ikotema.digital/andres).

Counseling and betatesting by Asano (淺野) and Larissa Yuri Oyadomari (親泊).

Audios from [LMMS](https://lmms.io) samples.

[NowRegular](https://fontlibrary.org/en/font/now) font by Alfredo Marco Pradil. 

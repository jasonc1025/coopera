import Vue from 'vue'
import {i18n} from '@/translations'
import App from './App'
import router from '@/router'
import AudioPlugin from '@/audio'
// import VueWebsocket from 'vue-websocket'
import VueWebsocket from './socket'
import GameMap from '@/components/GameMap'
import ChatPanel from '@/components/ChatPanel'
import RoomsList from '@/components/RoomsList'
import PlayerName from '@/components/PlayerName'
import MsgsList from '@/components/MsgsList'
import LangMenu from '@/components/LangMenu'

Vue.config.productionTip = false

// [jwc] orig commented out: Vue.use(VueWebsocket, 'ws://localhost:5000')
// Y Vue.use(VueWebsocket, 'ws://localhost:5000')
// [jwc] Vue.use(VueWebsocket, 'wss://coopera-server.ikotema.digital')
Vue.use(VueWebsocket, 'wss://coopera-server.ikotema.digital')

Vue.use(AudioPlugin)

Vue.component('rooms-list', RoomsList)
Vue.component('player-name', PlayerName)
Vue.component('game-map', GameMap)
Vue.component('chat-panel', ChatPanel)
Vue.component('msgs-list', MsgsList)
Vue.component('lang-menu', LangMenu)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,
  router,
  render: h => h(App)
})

import Vue from 'vue'
import VueI18n from 'vue-i18n'
import {safeGetLS, safeSetLS} from '@/utils'

Vue.use(VueI18n)

export var localeState = {
  locale: safeGetLS('locale')
}

// Translations for each language. Keys are the locale codes.
export var translations = {}

export const i18n = new VueI18n({
  locale: getLocale() ? getLocale() : 'en',
  silentTranslationWarn: true,
  messages: translations
})

// List with languages locales codes and language name.
export var langs = []

export function getLocale () {
  return localeState.locale
}

export function setLocale (locale) {
  i18n.locale = locale
  localeState.locale = locale
  safeSetLS('locale', locale)
}

// Imports all the languages from the yaml files in this folder.
function importAll (r) {
  r.keys().forEach(key => {
    var lang = r(key)
    var code = key.slice(2, -5)
    translations[code] = lang
    langs.push({
      code,
      name: lang.lang_name
    })
  })
}
importAll(require.context('.', false, /\.yaml$/))
